# About avc-binding-yaml

This project contains binding classes between YAML incoming streams and annotated Java interfaces.

We use Java 5's annotations.

Its parent project is [avc-commons-parent](https://gitlab.com/avcompris/avc-commons-parent/).

Project dependencies include:

  * [avc-commons-lang](https://gitlab.com/avcompris/avc-commons-lang/)
  * [avc-commons-testutil](https://gitlab.com/avcompris/avc-commons-testutil/)
  * [avc-binding-common](https://gitlab.com/avcompris/avc-binding-common/)
  * [avc-binding-dom](https://gitlab.com/avcompris/avc-binding-dom/)

This is the project home page, hosted on GitLab.

[API Documentation is here](https://maven.avcompris.com/avc-binding-yaml/apidocs/index.html).

There is a [Maven Generated Site.](https://maven.avcompris.com/avc-binding-yaml/)
