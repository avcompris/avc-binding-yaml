package net.avcompris.binding.yaml.avcProjects;

import net.avcompris.binding.annotation.XPath;

@XPath("/avc-workspace")
public interface AvcProjectsInfo {

	@XPath("hg | svn")
	Repository[] getRepositories();

	@XPath("hg[@repository = $arg0]")
	Repository[] getHgRepositoriesByUrl(String url);

	int sizeOfRepositories();

	interface Repository {

		@XPath(value = ".", function = "name()")
		String getType();

		@XPath("@repository")
		String getUrl();

		@XPath("@branch")
		String getBranch();

		@XPath("avc-projects/@*")
		AvcProject[] getProjects();

		int sizeOfProjects();
	}

	@XPath("ignore/avc-projects/@*")
	AvcProject[] getIgnoredProjects();

	interface AvcProject {

		@XPath(value = ".", function = "name()")
		String getName();

		@XPath(".")
		String getValue();
	}
}
