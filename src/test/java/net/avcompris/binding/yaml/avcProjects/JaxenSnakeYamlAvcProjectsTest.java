package net.avcompris.binding.yaml.avcProjects;

import static net.avcompris.binding.yaml.testutil.LoadYamlUtils.loadSnakeYaml;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import net.avcompris.binding.yaml.impl.JaxenYamlBinder;

import org.junit.Before;
import org.xml.sax.SAXException;

public class JaxenSnakeYamlAvcProjectsTest extends AbstractAvcProjectsTest {

	@Before
	public void setUp() throws Exception {

		setBinder(new JaxenYamlBinder());
	}

	@Override
	protected Object loadYaml(final String s) throws ParserConfigurationException, SAXException, IOException {

		return loadSnakeYaml(s);
	}
}
