package net.avcompris.binding.yaml.avcProjects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import net.avcompris.binding.yaml.testutil.AbstractYamlBinderTest;

import org.junit.Test;
import org.xml.sax.SAXException;

public abstract class AbstractAvcProjectsTest extends AbstractYamlBinderTest {

	@Test
	public final void test_loadInnerInterfaces_and_attributes() throws Exception {

		final AvcProjectsInfo a = binder.bind(
				loadYaml("src/test/yaml/003-avc-projects.yaml"),
				AvcProjectsInfo.class);

		assertEquals("hg", a.getRepositories()[0].getType());
		assertEquals("https://${avc.project}.googlecode.com/hg", a
				.getRepositories()[0].getUrl());
		assertEquals("trunk", a.getRepositories()[0].getBranch());

		assertEquals("hg", a.getRepositories()[2].getType());
		assertEquals("ssh://kimsufi//home/hg/projects/${avc.project}", a
				.getRepositories()[2].getUrl());
		assertEquals("default", a.getRepositories()[2].getBranch());

		assertEquals("avc-binding-dom", a.getRepositories()[2].getProjects()[9]
				.getName());
		assertEquals("true", a.getRepositories()[3].getProjects()[9].getValue());

		assertEquals(6, a.getRepositories()[0].getProjects().length);

		final Set<String> refs = new HashSet<String>(Arrays.asList(
				"avc-base-parent", "avc-base-testutil", "avc-base-testutil-ut",
				"avc-commons-lang", "avc-commons-parent",
				"avc-commons-testutil"));
		for (int i = 0; i < 6; ++i) {
			final String repo0_projectx_name = a.getRepositories()[0]
					.getProjects()[i].getName();
			assertTrue("repo0_project" + i + "_name: " + repo0_projectx_name,
					refs.contains(repo0_projectx_name));
			assertEquals("true", a.getRepositories()[0].getProjects()[i]
					.getValue());
			refs.remove(repo0_projectx_name);
		}

		assertEquals("avc-system", a.getRepositories()[3].getProjects()[69]
				.getName());
		assertEquals("false", a.getRepositories()[3].getProjects()[69]
				.getValue());
	}

	@Test
	public final void test_sizeOf() throws Exception {

		final AvcProjectsInfo a = binder.bind(
				loadYaml("src/test/yaml/003-avc-projects.yaml"),
				AvcProjectsInfo.class);

		assertEquals(5, a.sizeOfRepositories());
	}

	protected abstract Object loadYaml(String s) throws ParserConfigurationException, SAXException, IOException;

	@Test
	public final void test_XPathVariables() throws Exception {

		final AvcProjectsInfo a = binder.bind(
				loadYaml("src/test/yaml/003-avc-projects.yaml"),
				AvcProjectsInfo.class);

		assertEquals(5, a.getRepositories().length);

		assertEquals(0, a.getHgRepositoriesByUrl("toto").length);

		assertEquals(
				2,
				a
						.getHgRepositoriesByUrl("https://${avc.project}.googlecode.com/hg").length);
	}
}
