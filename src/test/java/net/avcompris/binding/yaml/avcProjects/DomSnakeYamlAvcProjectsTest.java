package net.avcompris.binding.yaml.avcProjects;

import static net.avcompris.binding.yaml.testutil.LoadYamlUtils.loadSnakeYaml;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import net.avcompris.binding.yaml.impl.DomYamlBinder;

import org.junit.Before;
import org.xml.sax.SAXException;

public class DomSnakeYamlAvcProjectsTest extends AbstractAvcProjectsTest {

	@Before
	public final void setUp() throws Exception {

		setBinder(new DomYamlBinder());
	}

	@Override
	protected Object loadYaml(final String s) throws ParserConfigurationException, SAXException, IOException {

		return loadSnakeYaml(s);
	}
}
