package net.avcompris.binding.yaml.constraints;

import java.io.IOException;

import net.avcompris.binding.yaml.impl.DomYamlBinder;
import net.avcompris.binding.yaml.testutil.LoadYamlUtils;

import org.junit.Before;

public class DomYamlBeansConstraintsConfigTest extends
		AbstractConstraintsConfigTest {

	@Before
	public void setUp() throws Exception {

		setBinder(new DomYamlBinder());

		super.setUp();
	}

	@Override
	protected Object loadYaml(final String filename) throws IOException {

		return LoadYamlUtils.loadYamlBeans(filename);
	}
}
