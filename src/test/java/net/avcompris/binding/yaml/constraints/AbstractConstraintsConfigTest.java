package net.avcompris.binding.yaml.constraints;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import net.avcompris.binding.yaml.constraints.ConstraintsConfig.ConfigElement.Property;
import net.avcompris.binding.yaml.testutil.AbstractYamlBinderTest;

import org.junit.*;
import static org.junit.Assert.*;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.google.common.collect.Sets;

public abstract class AbstractConstraintsConfigTest extends
		AbstractYamlBinderTest {

	@Before
	public void setUp() throws Exception {

		config = binder.bind(loadYaml("src/test/yaml/005-constraints.yaml"),
				ConstraintsConfig.class);
	}

	private ConstraintsConfig config;

	@Test
	public final void test_getFilename() throws Exception {

		assertEquals("AVDViewController_iPhone.xib", config.getFilename());
	}

	@Test
	public final void test_sizeOfConfigElements() throws Exception {

		assertEquals(11, config.getConfigElements().length);

		assertEquals(11, config.sizeOfConfigElements());
	}

	@Test
	public final void test_getConfigElementNames() throws Exception {

		assertEquals("LButton 20", config.getConfigElements()[0].getName());
		assertEquals("LButton +", config.getConfigElements()[1].getName());
		assertEquals("LButton -", config.getConfigElements()[2].getName());
		assertEquals("RButton 20", config.getConfigElements()[3].getName());
		assertEquals("RButton +", config.getConfigElements()[4].getName());
		assertEquals("LLabel +4 TL", config.getConfigElements()[5].getName());
		assertEquals("RLabel +4 TL", config.getConfigElements()[6].getName());
	}

	@Test
	public final void test_getConfigElement7Keys() throws Exception {

		assertEquals("RLabel +4 TL", config.getConfigElements()[6].getName());

		assertEquals(1, config.getConfigElements()[6].sizeOfNames());

		assertEquals(4, config.getConfigElements()[7].sizeOfNames());

		assertEquals("LLabel +4 BL",
				config.getConfigElements()[7].getNames()[0]);
		assertEquals("LLabel +4 BR",
				config.getConfigElements()[7].getNames()[1]);
		assertEquals("RLabel +4 BL",
				config.getConfigElements()[7].getNames()[2]);
		assertEquals("RLabel +4 BR",
				config.getConfigElements()[7].getNames()[3]);
	}

	@Test
	public final void test_getConfigElement0PropertyNames() throws Exception {

		assertEquals(5, config.getConfigElements()[0].sizeOfProperties());

		final Set<String> propertyNames=
		Sets.newHashSet(config.getConfigElements()[0].getProperties()[0].getName(),
				config.getConfigElements()[0].getProperties()[1].getName(),
				config.getConfigElements()[0].getProperties()[2].getName(),
				config.getConfigElements()[0].getProperties()[3].getName(),
				config.getConfigElements()[0].getProperties()[4].getName());
		
		assertTrue(propertyNames.contains("bottom"));
		assertTrue(propertyNames.contains("height"));
		assertTrue(propertyNames.contains("left"));
		assertTrue(propertyNames.contains("top"));
		assertTrue(propertyNames.contains("width"));
	}

	@Test
	public final void test_getConfigElementProperty0HeightValues() throws Exception {

		// "LButton 20":
		// height: { sameAs: "RButton 20", equals: 100 }

		final Property p = config.getConfigElements()[0]
				.getPropertyByName("height");

		assertEquals("RButton 20", p.getValueForExpression("sameAs"));
		assertEquals("100", p.getValueForExpression("equals"));

		assertEquals(1, p.sizeOfValuesForExpression("sameAs"));
		assertEquals(1, p.sizeOfValuesForExpression("equals"));

		assertEquals("RButton 20", p.getValuesForExpression("sameAs")[0]);
		assertEquals("100", p.getValuesForExpression("equals")[0]);
	}

	@Test
	public final void test_getConfigElementProperty0TopValues() throws Exception {

		// "LButton 20":
		// top: { sameAs: [ "RButton 20", "LButton +" ] }

		final Property p = config.getConfigElements()[0]
				.getPropertyByName("top");

		assertEquals("top", p.getName());

	//	assertEquals("", p.getValueForExpression("sameAs"));

		assertEquals(2, p.sizeOfValuesForExpression("sameAs"));
		assertEquals("RButton 20", p.getValuesForExpression("sameAs")[0]);
		assertEquals("LButton +", p.getValuesForExpression("sameAs")[1]);
	}

	protected abstract Object loadYaml(String s) throws ParserConfigurationException, SAXException, IOException;
}
