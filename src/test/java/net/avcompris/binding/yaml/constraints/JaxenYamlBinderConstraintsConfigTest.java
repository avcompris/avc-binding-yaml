package net.avcompris.binding.yaml.constraints;

import java.io.IOException;

import net.avcompris.binding.yaml.impl.JaxenYamlBinder;
import net.avcompris.binding.yaml.testutil.LoadYamlUtils;

import org.junit.Before;
import org.junit.Ignore;

@Ignore
public class JaxenYamlBinderConstraintsConfigTest extends
		AbstractConstraintsConfigTest {

	@Before
	public void setUp() throws Exception {

		setBinder(new JaxenYamlBinder());

		super.setUp();
	}

	@Override
	protected Object loadYaml(final String filename) throws IOException {

		return LoadYamlUtils.loadJvyamlb(filename);
	}
}
