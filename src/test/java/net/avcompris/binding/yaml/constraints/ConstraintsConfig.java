package net.avcompris.binding.yaml.constraints;

import net.avcompris.binding.annotation.Namespaces;
import net.avcompris.binding.annotation.Nodes;
import net.avcompris.binding.annotation.XPath;

@XPath("/*")
@Namespaces("xmlns:avc=http://avc-binding-yaml.googlecode.com/")
@Nodes(elementsEverywhere = true, elementNames=false)
public interface ConstraintsConfig {

	@XPath("name()")
	String getFilename();
	
	@XPath("avc:item/avc:key")
	ConfigElement[] getConfigElements();	
	int sizeOfConfigElements();
	
	@Namespaces("xmlns:avc=http://avc-binding-yaml.googlecode.com/")
	interface ConfigElement {
		
		@XPath("@name")
		String getName();

		//@XPath(value="self::*", function="name(*)")
		//String getName2();
		//int sizeOfName2();

		@XPath("@name | self::avc:key[not(@name)]/avc:item")//, function="name()")
		String[] getNames();
		int sizeOfNames();
		
		@XPath("avc:key | avc:value/avc:key")
		Property[] getProperties();
		int sizeOfProperties();

		@XPath("avc:key[@name = $arg0]")// | avc:value/avc:key[@name = $arg0]")
		Property getPropertyByName(String name);

		/*
	<key name="top">
        <key name="sameAs">
            <item>RButton 20</item>
            <item>LButton +</item>
        </key>
    </key>*/
    
		@Namespaces("xmlns:avc=http://avc-binding-yaml.googlecode.com/")
		interface Property {
			
			@XPath("@name")
			String getName();
			
			@XPath("avc:key[@name = $arg0]")
			String getValueForExpression(String expression);
			
			@XPath("avc:key[@name = $arg0 and not(avc:item)] | avc:key[@name = $arg0]/avc:item")
			String[] getValuesForExpression(String expression);
			int sizeOfValuesForExpression(String expression);
		}
	}
}
