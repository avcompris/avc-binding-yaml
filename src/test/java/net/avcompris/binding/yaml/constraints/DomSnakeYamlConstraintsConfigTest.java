package net.avcompris.binding.yaml.constraints;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import net.avcompris.binding.yaml.impl.DomYamlBinder;
import net.avcompris.binding.yaml.testutil.LoadYamlUtils;

import org.junit.Before;
import org.xml.sax.SAXException;

public class DomSnakeYamlConstraintsConfigTest extends
		AbstractConstraintsConfigTest {

	@Before
	public void setUp() throws Exception {

		setBinder(new DomYamlBinder());

		super.setUp();
	}

	@Override
	protected Object loadYaml(final String filename)
			throws ParserConfigurationException, SAXException, IOException {

		return LoadYamlUtils.loadSnakeYaml(filename);
	}
}
