package net.avcompris.binding.yaml;

import java.util.Collection;

import net.avcompris.binding.BindConfiguration;
import net.avcompris.binding.yaml.impl.DomYamlBinder;
import net.avcompris.binding.yaml.impl.DomYamlConversionException;
import net.avcompris.binding.yaml.impl.DomYamlConverter;
import net.avcompris.binding.yaml.impl.JaxenYamlBinder;
import net.avcompris.jaxen.yaml.YamlXPath;

import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized.Parameters;

import com.avcompris.common.annotation.Nullable;
import com.avcompris.util.junit.AvcParameterized;
import com.avcompris.util.reflect.AbstractNonNullTest;

/**
 * tests on Java classes in the "<code>net.avcompris.binding.yaml</code>" package, to check
 * if the method parameters that are not flagged
 * as {@link Nullable} are checked for <code>null</code>-values.
 * 
 * @author David Andrianavalontsalama 2011 ©
 */
@RunWith(AvcParameterized.class)
@Ignore
public class AvcBindingYamlNonNullTest extends AbstractNonNullTest {

	/**
	 * constructor.
	 * 
	 * @param holder the instance+member to test
	 */
	public AvcBindingYamlNonNullTest(final MemberHolder holder)
			throws Exception {

		super(holder);
	}

	/**
	 * @return all the Java methods to test.
	 */
	@Parameters
	public static Collection<?> parameters() throws Exception {

		return parametersScanProject(YamlBindingException.class,
				DomYamlBinder.class, DomYamlConversionException.class,
				DomYamlConverter.class, JaxenYamlBinder.class, YamlXPath.class,
				BindConfiguration.newBuilder().build());
	}
}
