package net.avcompris.binding.yaml.basic;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import net.avcompris.binding.annotation.XPath;

import org.junit.Before;
import org.junit.Test;

public abstract class AbstractStringArrayTest {

	protected abstract Andrew getAndrew() throws IOException;

	@Before
	public final void setUp() throws Exception {

		andrew = getAndrew();
	}

	private Andrew andrew = null;

	@Test
	public final void testStringArray() throws Exception {

		assertEquals(2, andrew.sizeOfChildren());
		assertEquals(2, andrew.sizeOfParents());

		final String[] children = andrew.getChildren();
		final Person[] parents = andrew.getParents();

		assertEquals(2, children.length);

		assertEquals("Albert", children[0]);
		assertEquals("Bernie", children[1]);

		assertEquals(2, parents.length);

		assertEquals("X", andrew.getLastName());
	}

	@Test
	public final void testStringArraySub() throws Exception {

		assertEquals(2, andrew.sizeOfParents());

		final Person[] parents = andrew.getParents();

		assertEquals(2, parents.length);

		assertEquals("Samantha", parents[0].getName());
		assertEquals("Ludovic", parents[1].getName());

		assertEquals(2, parents[0].sizeOfChildren());
		assertEquals("Andrew", parents[0].getChildren()[0]);
		assertEquals("Xeres", parents[0].getChildren()[1]);

		assertEquals(1, parents[1].sizeOfChildren());
		assertEquals("Andrew", parents[1].getChildren()[0]);
	}

	@XPath("/Andrew")
	protected interface Andrew extends Person {

	}

	@XPath("/*")
	protected interface Person {

		@XPath("children")
		String[] getChildren();

		int sizeOfChildren();

		@XPath("parent")
		Person[] getParents();

		int sizeOfParents();

		@XPath("lastName")
		String getLastName();

		@XPath("name")
		String getName();
	}
}
