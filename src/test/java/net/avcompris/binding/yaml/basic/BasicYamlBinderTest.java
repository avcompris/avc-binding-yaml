package net.avcompris.binding.yaml.basic;

import static com.avcompris.util.junit.JUnitUtils.createTmpFileFromCommentsAroundThisMethod;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import net.avcompris.binding.annotation.Nodes;
import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.yaml.YamlBinder;
import net.avcompris.binding.yaml.impl.DomYamlBinder;
import net.avcompris.binding.yaml.impl.JaxenYamlBinder;

import org.junit.Ignore;
import org.junit.Test;

import com.avcompris.util.SnakeYAMLUtils;
import com.avcompris.util.YamlUtils;

public class BasicYamlBinderTest {

	@Test
	public void testBasic() throws Exception {

		final Object yaml = SnakeYAMLUtils
				.loadYAML(createTmpFileFromCommentsAroundThisMethod());

		// person:
		//   firstName: Arthur
		//   lastName: Miller

		final YamlBinder binder = new JaxenYamlBinder();

		final Person person = binder.bind(yaml, Person.class);

		assertEquals("Miller", person.getLastName());

		assertEquals("Arthur", person.getFirstName());
	}

	@Test
	public void testJaxenBoolean_true() throws Exception {

		final Object yaml = SnakeYAMLUtils
				.loadYAML(createTmpFileFromCommentsAroundThisMethod());

		// person:
		//   firstName: Arthur
		//   lastName: Miller
		//   male: true

		final YamlBinder binder = new JaxenYamlBinder();

		final Person person = binder.bind(yaml, Person.class);

		assertEquals("Arthur",person.getFirstName());
		assertTrue(person.isMale());
	}

	@Test
	public void testDomBoolean_false() throws Exception {

		final Object yaml = SnakeYAMLUtils
				.loadYAML(createTmpFileFromCommentsAroundThisMethod());

		// person:
		//   firstName: Wanda
		//   male: false

		final YamlBinder binder = new DomYamlBinder();

		final Person person = binder.bind(yaml, Person.class);

		assertEquals("Wanda",person.getFirstName());
		assertFalse(person.isMale());
	}

	@Test
	public void testDomBoolean_true() throws Exception {

		final Object yaml = SnakeYAMLUtils
				.loadYAML(createTmpFileFromCommentsAroundThisMethod());

		// person:
		//   firstName: Arthur
		//   lastName: Miller
		//   male: true

		final YamlBinder binder = new DomYamlBinder();

		final Person person = binder.bind(yaml, Person.class);

		assertEquals("Arthur",person.getFirstName());
		assertTrue(person.isMale());
	}

	@Test
	public void testJaxenBoolean_false() throws Exception {

		final Object yaml = SnakeYAMLUtils
				.loadYAML(createTmpFileFromCommentsAroundThisMethod());

		// person:
		//   firstName: Wanda
		//   male: false

		final YamlBinder binder = new JaxenYamlBinder();

		final Person person = binder.bind(yaml, Person.class);

		assertEquals("Wanda",person.getFirstName());
		assertFalse(person.isMale());
	}

	@XPath("/person")
	private interface Person {

		@XPath("@firstName")
		String getFirstName();

		@XPath("@lastName")
		String getLastName();

		@XPath("@male")
		boolean isMale();
	}

	@XPath("/")
	@Nodes(elementsEverywhere = true)
	private interface PersonAtRoot {

		@XPath("firstName")
		String getFirstName();

		@XPath("lastName")
		String getLastName();
	}

	@Test
	public void testTwoRoots() throws Exception {

		final Object yaml = SnakeYAMLUtils
				.loadYAML(createTmpFileFromCommentsAroundThisMethod());

		// person:
		//   firstName: Conan
		//   lastName: Doyle
		//
		// book:
		//   title: A Study in Scarlet
		//   year: 1887

		final YamlBinder binder = new JaxenYamlBinder();

		final Person person = binder.bind(yaml, Person.class);

		assertEquals("Doyle", person.getLastName());

		assertEquals("Conan", person.getFirstName());

		final Book book = binder.bind(yaml, Book.class);

		assertEquals("A Study in Scarlet", book.getTitle());
	}

	@Test
	public void testTwoAttributesAtRoot() throws Exception {

		final Object yaml = SnakeYAMLUtils
				.loadYAML(createTmpFileFromCommentsAroundThisMethod());

		// firstName: Conan
		// lastName: Doyle

		final YamlBinder binder = new JaxenYamlBinder();

		final PersonAtRoot person = binder.bind(yaml, PersonAtRoot.class);

		assertEquals("Doyle", person.getLastName());

		assertEquals("Conan", person.getFirstName());
	}

	@XPath("/book")
	private interface Book {

		@XPath("@title")
		String getTitle();
	}

	@Test
	@Ignore
	// TODO
	public void testTwoStreams() throws Exception {

		final Object yaml = YamlUtils
				.loadYAML(createTmpFileFromCommentsAroundThisMethod());

		// person:
		//   firstName: Oscar
		//   lastName: Wilde
		//
		// ---
		//
		// book:
		//   title: The Importance of Being Earnest
		//   year: 1895

		final YamlBinder binder = new JaxenYamlBinder();

		final Person person = binder.bind(yaml, Person.class);

		assertEquals("Wilde", person.getLastName());

		assertEquals("Oscar", person.getFirstName());

		final Book book = binder.bind(yaml, Book.class);

		assertEquals("The Importance of Being Earnest", book.getTitle());
	}
}
