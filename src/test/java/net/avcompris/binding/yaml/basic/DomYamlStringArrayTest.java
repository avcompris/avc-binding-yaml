package net.avcompris.binding.yaml.basic;

import java.io.File;
import java.io.IOException;

import net.avcompris.binding.BindConfiguration;
import net.avcompris.binding.yaml.YamlBinder;
import net.avcompris.binding.yaml.impl.DomYamlBinder;

import com.avcompris.util.SnakeYAMLUtils;

public class DomYamlStringArrayTest extends AbstractStringArrayTest {

	@Override
	protected Andrew getAndrew() throws IOException {

		final Object yaml = SnakeYAMLUtils.loadYAML(new File("src/test/yaml",
				"006-stringArrays.yaml"));

		final YamlBinder binder = new DomYamlBinder();

		final Andrew andrew = binder.bind(BindConfiguration.newBuilder()
				.setNodesElementsEverywhere(true).build(), yaml, Andrew.class);

		return andrew;
	}
}
