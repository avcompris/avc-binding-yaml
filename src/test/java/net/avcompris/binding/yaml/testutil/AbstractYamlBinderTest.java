package net.avcompris.binding.yaml.testutil;

import net.avcompris.binding.yaml.YamlBinder;

import org.junit.Before;

public abstract class AbstractYamlBinderTest {

	protected final void setBinder(final YamlBinder binder) {

		this.binder = binder;
	}

	@Before
	public final void setUpNullBinder() {

		binder = null;
	}

	protected YamlBinder binder;
}
