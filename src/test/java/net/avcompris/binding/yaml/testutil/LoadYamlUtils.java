package net.avcompris.binding.yaml.testutil;

import static org.apache.commons.lang3.CharEncoding.UTF_8;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.xml.parsers.ParserConfigurationException;

import org.jvyaml.YAML;
import org.xml.sax.SAXException;
import org.yaml.snakeyaml.Yaml;

import com.avcompris.util.AbstractUtils;
import com.esotericsoftware.yamlbeans.YamlReader;

/**
 * utility methods for tests on avc-binding-yaml.
 */
public class LoadYamlUtils extends AbstractUtils {

	public static Object loadSnakeYaml(final String filename) throws ParserConfigurationException, SAXException, IOException {

		final Yaml yaml = new Yaml();

		final InputStream is = new FileInputStream(filename);
		try {

			return yaml.load(is);

		} finally {
			is.close();
		}
	}

	public static Object loadYamlBeans(final String filename) throws IOException {

		final InputStream is = new FileInputStream(filename);
		try {

			final Reader reader = new InputStreamReader(is, UTF_8);

			final YamlReader yamlReader = new YamlReader(reader);

			return yamlReader.read();

		} finally {
			is.close();
		}
	}

	public static Object loadJvyamlb(final String filename) throws IOException {

		final InputStream is = new FileInputStream(filename);
		try {

			final Reader reader = new InputStreamReader(is, UTF_8);

			return YAML.load(reader);

		} finally {
			is.close();
		}
	}
}
