package net.avcompris.binding.yaml.logs;

import net.avcompris.binding.annotation.XPath;

import org.joda.time.DateTime;

/**
 * this interface represents log lines fetched via the Adm Web Service.
 * Both <code>DomYamlBinder</code> and <code>JaxenYamlBinder</code> may be used
 * to bind this interface (properties are load through attribute axis).
 *
 * @author David Andrianavalontsalama
 */
@XPath("/logs")
public interface FetchedLogs {

	@XPath("@count")
	int getCount();

	@XPath("requests/@count")
	int getRequestCount();

	@XPath("requests/@minId")
	int getRequestMinId();

	boolean isNullRequestMinId();

	@XPath("requests/@*[name() = 'minRequestAt']")
	DateTime getRequestMinRequestAt();

	@XPath("requests/@maxId")
	int getRequestMaxId();

	boolean isNullRequestMaxId();

	@XPath("requests/@*[name() = 'maxRequestAt']")
	DateTime getRequestMaxRequestAt();

	@XPath("sessions/@count")
	int getSessionCount();

	@XPath("requests/*")
	FetchedRequest[] getRequests();

	int sizeOfRequests();
}
