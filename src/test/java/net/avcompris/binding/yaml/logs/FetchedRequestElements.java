package net.avcompris.binding.yaml.logs;

import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.annotation.Nodes;

import org.joda.time.DateTime;

/**
 * this interface represents a request fetched via the Adm Web Service,
 * and differs from {@link FetchedRequest} in that properties are
 * loaded by element axis, not attributes. <code>JaxenYamlBinder</code> is intended
 * for such a case.
 *
 * @author David Andrianavalontsalama
 */
@XPath("/logs/requests/*[name() = 'request_items']")
@Nodes(elementsEverywhere = true)
public interface FetchedRequestElements {

	@XPath("id")
	int getId();

	@XPath("at")
	DateTime getAt();

	@XPath("elapsedMs")
	int getElapsedMs();

	boolean isNullElapsedMs();

	@XPath("sessionId")
	int getSessionId();

	boolean isNullSessionId();

	@XPath("*[name() = 'log_items']")
	FetchedLogLine[] getLogLines();

	int sizeOfLogLines();
}
