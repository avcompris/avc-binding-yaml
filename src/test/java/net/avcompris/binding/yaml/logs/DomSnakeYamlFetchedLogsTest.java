package net.avcompris.binding.yaml.logs;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import net.avcompris.binding.yaml.impl.DomYamlBinder;
import net.avcompris.binding.yaml.testutil.LoadYamlUtils;

import org.junit.Before;
import org.xml.sax.SAXException;

public class DomSnakeYamlFetchedLogsTest extends AbstractFetchedLogsTest {

	@Before
	public void setUp() throws Exception {

		setBinder(new DomYamlBinder());
	}

	@Override
	protected Object loadYaml(final String filename) throws ParserConfigurationException, SAXException, IOException {

		return LoadYamlUtils.loadSnakeYaml(filename);
	}
}
