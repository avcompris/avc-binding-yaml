package net.avcompris.binding.yaml.logs;

import java.io.IOException;

import net.avcompris.binding.yaml.impl.DomYamlBinder;
import net.avcompris.binding.yaml.testutil.LoadYamlUtils;

import org.junit.Before;

public class DomYamlBeansFetchedLogsTest extends AbstractFetchedLogsTest {

	@Before
	public void setUp() throws Exception {

		setBinder(new DomYamlBinder());
	}

	@Override
	protected Object loadYaml(final String filename) throws IOException {

		return LoadYamlUtils.loadYamlBeans(filename);
	}
}
