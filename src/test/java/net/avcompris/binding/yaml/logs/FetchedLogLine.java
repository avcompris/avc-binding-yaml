package net.avcompris.binding.yaml.logs;

import net.avcompris.binding.annotation.XPath;

/**
 * this interface represents a log line fetched via the Adm Web Service.
 * Both <code>DomYamlBinder</code> and <code>JaxenYamlBinder</code> may be used
 * to bind this interface (properties are load through attribute axis).
 *
 * @author David Andrianavalontsalama
 */
@XPath("/logs/requests/*/*")
public interface FetchedLogLine {

	@XPath("@range")
	int getRange();

	@XPath("@level")
	char getLevel();

	@XPath("@elapsedMs")
	int getElapsedMs();

	@XPath("@msgTruncated")
	String getMsgTruncated();
}
