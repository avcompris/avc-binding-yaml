package net.avcompris.binding.yaml.logs;

import static org.joda.time.DateTimeZone.UTC;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import net.avcompris.binding.BindConfiguration;
import net.avcompris.binding.yaml.testutil.AbstractYamlBinderTest;

import org.joda.time.DateTime;
import org.junit.Test;
import org.xml.sax.SAXException;

public abstract class AbstractFetchedLogsTest extends AbstractYamlBinderTest {

	@Test
	public final void test_getLogCount() throws Exception {

		final FetchedLogs logs = binder.bind(
				loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedLogs.class);

		assertEquals(375, logs.getCount());
	}

	@Test
	public final void test_otherXPath_getLogCount() throws Exception {

		final FetchedLogs logs = binder.bind(
				BindConfiguration.newBuilder("/logs/requests").build(),
				loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedLogs.class);

		assertEquals(38, logs.getCount());
	}

	@Test
	public final void test_otherXPath_getMsgTruncated() throws Exception {

		final FetchedLogLine logLine = binder.bind(BindConfiguration
				.newBuilder("/logs/requests/*[1]/*[@range = 4]").build(),
				loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedLogLine.class);

		assertEquals(4, logLine.getRange());
		assertEquals('I', logLine.getLevel());
		assertEquals(1710, logLine.getElapsedMs());
		assertEquals("HTTP_HOST: localhost", logLine.getMsgTruncated());
	}

	@Test
	public final void test_getDateTime() throws Exception {

		final FetchedLogs logs = binder.bind(
				loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedLogs.class);

		assertEquals(new DateTime(2011, 11, 25, 0, 50, 4, UTC),
				logs.getRequestMinRequestAt());
	}

	@Test
	public final void test_otherXPath_hasElapsedMs() throws Exception {

		final FetchedRequest request5 = binder.bind(BindConfiguration
				.newBuilder("/logs/requests/*[@id = 5]").build(),
				loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedRequest.class);

		assertEquals(5, request5.getId());
		assertFalse(request5.isNullElapsedMs());
		assertEquals(1903, request5.getElapsedMs());

		final FetchedRequest request8 = binder.bind(BindConfiguration
				.newBuilder("/logs/requests/*[@id = 8]").build(),
				loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedRequest.class);

		assertEquals(8, request8.getId());
		assertTrue(request8.isNullElapsedMs());
		assertEquals(0, request8.getElapsedMs());
	}

	@Test
	public final void test_sizeOfRequests() throws Exception {

		final FetchedLogs logs = binder.bind(
				loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedLogs.class);

		assertEquals(6, logs.sizeOfRequests());
	}

	@Test
	public final void test_getRequests() throws Exception {

		final FetchedLogs logs = binder.bind(
				loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedLogs.class);

		final FetchedRequest[] requests = logs.getRequests();

		assertEquals(6, requests.length);

		assertEquals(5, requests[0].getId());
		assertEquals(8, requests[3].getId());
	}

	@Test
	public final void test_getRequests_sizeOfLogLines() throws Exception {

		final FetchedLogs logs = binder.bind(
				loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedLogs.class);

		final FetchedRequest[] requests = logs.getRequests();

		assertEquals(6, requests.length);

		assertEquals(5, requests[0].getId());
		assertEquals(9, requests[0].sizeOfLogLines());

		assertEquals(8, requests[3].getId());
		assertEquals(12, requests[3].sizeOfLogLines());
	}

	@Test
	public final void test_getRequests_getLogLines() throws Exception {

		final FetchedLogs logs = binder.bind(
				loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedLogs.class);

		final FetchedRequest[] requests = logs.getRequests();

		assertEquals(6, requests.length);

		assertEquals(5, requests[0].getId());
		assertEquals("PHP_SELF: /tmp2/index.php",
				requests[0].getLogLines()[8].getMsgTruncated());

		assertEquals(8, requests[3].getId());
		assertEquals("PHP_SELF: /tmp2/404.php",
				requests[3].getLogLines()[8].getMsgTruncated());
	}

	protected abstract Object loadYaml(String s) throws ParserConfigurationException, SAXException, IOException;
}
