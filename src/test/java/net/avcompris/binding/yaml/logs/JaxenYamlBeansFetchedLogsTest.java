package net.avcompris.binding.yaml.logs;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import net.avcompris.binding.yaml.testutil.LoadYamlUtils;

import org.xml.sax.SAXException;

public class JaxenYamlBeansFetchedLogsTest extends
		AbstractJaxenYamlFetchedLogsTest {

	@Override
	protected Object loadYaml(final String filename) throws ParserConfigurationException, SAXException, IOException {

		return LoadYamlUtils.loadYamlBeans(filename);
	}
}
