package net.avcompris.binding.yaml.logs;

import net.avcompris.binding.annotation.XPath;

/**
 * this interface represents a log line fetched via the Adm Web Service,
 * and differs from {@link FetchedLogLine} in that property names may
 * contain whitespaces. <code>JaxenYamlBinder</code> is intended
 * for such a case.
 *
 * @author David Andrianavalontsalama
 */
@XPath("/logs/requests/*[name() = 'request items']/*[name() = 'log items']")
public interface FetchedLogLineNamesWithWhitespaces {

	@XPath("@range")
	int getRange();

	@XPath("@level")
	char getLevel();

	@XPath("@elapsedMs")
	int getElapsedMs();

	@XPath("@msgTruncated")
	String getMsgTruncated();
}
