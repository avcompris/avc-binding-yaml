package net.avcompris.binding.yaml.logs;

import net.avcompris.binding.annotation.XPath;

import org.joda.time.DateTime;

/**
 * this interface represents a request fetched via the Adm Web Service,
 * and differs from {@link FetchedRequest} in that property names may
 * contain whitespaces. <code>JaxenYamlBinder</code> is intended
 * for such a case.
 *
 * @author David Andrianavalontsalama
 */
@XPath("/logs/requests/*[name() = 'request items']")
public interface FetchedRequestNamesWithWhitespaces {

	@XPath("@id")
	int getId();

	@XPath("@at")
	DateTime getAt();

	@XPath("@elapsedMs")
	int getElapsedMs();

	boolean isNullElapsedMs();

	@XPath("@elapsedMs")
	String getElapsedMsAsString();

	@XPath("@sessionId")
	int getSessionId();

	boolean isNullSessionId();

	@XPath("*[name() = 'log items']")
	FetchedLogLineNamesWithWhitespaces[] getLogLines();

	int sizeOfLogLines();
}
