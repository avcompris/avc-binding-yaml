package net.avcompris.binding.yaml.logs;

import net.avcompris.binding.annotation.XPath;

import org.joda.time.DateTime;

/**
 * this interface represents a request fetched via the Adm Web Service.
 * Both <code>DomYamlBinder</code> and <code>JaxenYamlBinder</code> may be used
 * to bind this interface (properties are load through attribute axis).
 *
 * @author David Andrianavalontsalama
 */
@XPath("/logs/requests/*[name() = 'request_items']")
public interface FetchedRequest {

	@XPath("@id")
	int getId();

	@XPath("@at")
	DateTime getAt();

	@XPath("@elapsedMs")
	int getElapsedMs();

	boolean isNullElapsedMs();

	@XPath("@sessionId")
	int getSessionId();

	boolean isNullSessionId();

	@XPath("*[name() = 'log_items']")
	FetchedLogLine[] getLogLines();

	int sizeOfLogLines();
}
