package net.avcompris.binding.yaml.logs;

import static org.joda.time.DateTimeZone.UTC;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import net.avcompris.binding.BindConfiguration;
import net.avcompris.binding.yaml.impl.JaxenYamlBinder;

import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

public abstract class AbstractJaxenYamlFetchedLogsTest extends
		AbstractFetchedLogsTest {

	@Before
	public final void setUp() throws Exception {

		setBinder(new JaxenYamlBinder());
	}

	@Test
	public final void test_getLogCount_namesWithWhitespaces() throws Exception {

		final Object yaml = loadYaml("src/test/yaml/004-fetched-logs-names-with-whitespaces.yaml");

		final FetchedLogsNamesWithWhitespaces logs = binder.bind(yaml,
				FetchedLogsNamesWithWhitespaces.class);

		assertEquals(375, logs.getCount());
	}

	@Test
	public final void test_otherXPath_getLogCount_namesWithWhitespaces() throws Exception {

		final FetchedLogsNamesWithWhitespaces logs = binder
				.bind(BindConfiguration.newBuilder("/logs/requests").build(),
						loadYaml("src/test/yaml/004-fetched-logs-names-with-whitespaces.yaml"),
						FetchedLogsNamesWithWhitespaces.class);

		assertEquals(38, logs.getCount());
	}

	@Test
	public final void test_otherXPath_getMsgTruncated_namesWithWhitespaces() throws Exception {

		final FetchedLogLineNamesWithWhitespaces logLine = binder
				.bind(BindConfiguration.newBuilder(
						"/logs/requests/*[1]/*[@range = 4]").build(),
						loadYaml("src/test/yaml/004-fetched-logs-names-with-whitespaces.yaml"),
						FetchedLogLineNamesWithWhitespaces.class);

		assertEquals(4, logLine.getRange());
		assertEquals('I', logLine.getLevel());
		assertEquals(1710, logLine.getElapsedMs());
		assertEquals("HTTP_HOST: localhost", logLine.getMsgTruncated());
	}

	@Test
	public final void test_getDateTime_namesWithWhitespaces() throws Exception {

		final FetchedLogsNamesWithWhitespaces logs = binder
				.bind(loadYaml("src/test/yaml/004-fetched-logs-names-with-whitespaces.yaml"),
						FetchedLogsNamesWithWhitespaces.class);

		assertEquals(new DateTime(2011, 11, 25, 0, 50, 4, UTC),
				logs.getRequestMinRequestAt());
	}

	@Test
	public final void test_otherXPath_hasElapsedMs_namesWithWhitespaces() throws Exception {

		final FetchedRequestNamesWithWhitespaces request5 = binder
				.bind(BindConfiguration.newBuilder("/logs/requests/*[@id = 5]")
						.build(),
						loadYaml("src/test/yaml/004-fetched-logs-names-with-whitespaces.yaml"),
						FetchedRequestNamesWithWhitespaces.class);

		assertEquals(5, request5.getId());
		assertFalse(request5.isNullElapsedMs());
		assertEquals(1903, request5.getElapsedMs());
		assertEquals("1903", request5.getElapsedMsAsString());

		final FetchedRequestNamesWithWhitespaces request8 = binder
				.bind(BindConfiguration.newBuilder("/logs/requests/*[@id = 8]")
						.build(),
						loadYaml("src/test/yaml/004-fetched-logs-names-with-whitespaces.yaml"),
						FetchedRequestNamesWithWhitespaces.class);

		assertEquals(8, request8.getId());
		assertEquals(0, request8.getElapsedMs());

		final String elapsedMsAsString = request8.getElapsedMsAsString();

		if ("null".equals(elapsedMsAsString)) {

			// TODO: YamlBeans only!

			assertEquals("null", elapsedMsAsString);
			assertFalse(request8.isNullElapsedMs());

		} else {

			assertEquals("", elapsedMsAsString);
			assertTrue(request8.isNullElapsedMs());
		}
	}

	@Test
	public final void test_sizeOfRequests_namesWithWhitespaces() throws Exception {

		final FetchedLogsNamesWithWhitespaces logs = binder
				.bind(loadYaml("src/test/yaml/004-fetched-logs-names-with-whitespaces.yaml"),
						FetchedLogsNamesWithWhitespaces.class);

		assertEquals(6, logs.sizeOfRequests());
	}

	@Test
	public final void test_getRequests_namesWithWhitespaces() throws Exception {

		final FetchedLogsNamesWithWhitespaces logs = binder
				.bind(loadYaml("src/test/yaml/004-fetched-logs-names-with-whitespaces.yaml"),
						FetchedLogsNamesWithWhitespaces.class);

		final FetchedRequestNamesWithWhitespaces[] requests = logs
				.getRequests();

		assertEquals(6, requests.length);

		assertEquals(5, requests[0].getId());
		assertEquals(8, requests[3].getId());
	}

	@Test
	public final void test_getRequests_sizeOfLogLines_namesWithWhitespaces() throws Exception {

		final FetchedLogsNamesWithWhitespaces logs = binder
				.bind(loadYaml("src/test/yaml/004-fetched-logs-names-with-whitespaces.yaml"),
						FetchedLogsNamesWithWhitespaces.class);

		final FetchedRequestNamesWithWhitespaces[] requests = logs
				.getRequests();

		assertEquals(6, requests.length);

		assertEquals(5, requests[0].getId());
		assertEquals(9, requests[0].sizeOfLogLines());

		assertEquals(8, requests[3].getId());
		assertEquals(12, requests[3].sizeOfLogLines());
	}

	@Test
	public final void test_getRequests_getLogLines_namesWithWhitespaces() throws Exception {

		final FetchedLogsNamesWithWhitespaces logs = binder
				.bind(loadYaml("src/test/yaml/004-fetched-logs-names-with-whitespaces.yaml"),
						FetchedLogsNamesWithWhitespaces.class);

		final FetchedRequestNamesWithWhitespaces[] requests = logs
				.getRequests();

		assertEquals(6, requests.length);

		assertEquals(5, requests[0].getId());
		assertEquals("PHP_SELF: /tmp2/index.php",
				requests[0].getLogLines()[8].getMsgTruncated());

		assertEquals(8, requests[3].getId());
		assertEquals("PHP_SELF: /tmp2/404.php",
				requests[3].getLogLines()[8].getMsgTruncated());
	}

	@Test
	public final void test_getLogCount_elements() throws Exception {

		final FetchedLogsElements logs = binder.bind(
				loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedLogsElements.class);

		assertEquals(375, logs.getCount());
	}

	@Test
	public final void test_otherXPath_getLogCount_elements() throws Exception {

		final FetchedLogsElements logs = binder.bind(BindConfiguration
				.newBuilder("/logs/requests").setNodesElementsEverywhere(true)
				.build(), loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedLogsElements.class);

		assertEquals(38, logs.getCount());
	}

	@Test
	public final void test_otherXPath_getMsgTruncated_elements() throws Exception {

		final FetchedLogsElements logs = binder.bind(BindConfiguration
				.newBuilder("/logs").setNodesElementsEverywhere(true).build(),
				loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedLogsElements.class);

		assertEquals(11, logs.sizeOfRawRequestSubs());

		final String[] rawRequestSubs = logs.getRawRequestSubs();

		assertEquals(11, rawRequestSubs.length);

		assertEquals("count", rawRequestSubs[0]);
		assertEquals("maxId", rawRequestSubs[1]);
		assertEquals("maxRequestAt", rawRequestSubs[2]);
		assertEquals("minId", rawRequestSubs[3]);
		assertEquals("minRequestAt", rawRequestSubs[4]);
		assertEquals("request_items", rawRequestSubs[5]);
		assertEquals("request_items", rawRequestSubs[6]);
		assertEquals("request_items", rawRequestSubs[7]);
		assertEquals("request_items", rawRequestSubs[8]);
		assertEquals("request_items", rawRequestSubs[9]);
		assertEquals("request_items", rawRequestSubs[10]);

		final FetchedLogLineElements logLine = binder.bind(BindConfiguration
				.newBuilder("/logs/requests/*[6]/*[@range = 4]")
				.setNodesElementsEverywhere(true).build(),
				loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedLogLineElements.class);

		assertEquals(4, logLine.getRange());
		assertEquals('I', logLine.getLevel());
		assertEquals(1710, logLine.getElapsedMs());
		assertEquals("HTTP_HOST: localhost", logLine.getMsgTruncated());
	}

	@Test
	public final void test_getDateTime_elements() throws Exception {

		final FetchedLogsElements logs = binder.bind(
				loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedLogsElements.class);

		assertEquals(new DateTime(2011, 11, 25, 0, 50, 4, UTC),
				logs.getRequestMinRequestAt());
	}

	@Test
	public final void test_otherXPath_hasElapsedMs_elements() throws Exception {

		final FetchedRequest request5 = binder.bind(BindConfiguration
				.newBuilder("/logs/requests/*[@id = 5]")
				.setNodesElementsEverywhere(true).build(),
				loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedRequest.class);

		assertEquals(5, request5.getId());
		assertFalse(request5.isNullElapsedMs());
		assertEquals(1903, request5.getElapsedMs());

		final FetchedRequestElements request8 = binder.bind(BindConfiguration
				.newBuilder("/logs/requests/*[@id = 8]")
				.setNodesElementsEverywhere(true).build(),
				loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedRequestElements.class);

		assertEquals(8, request8.getId());
		assertTrue(request8.isNullElapsedMs());
		assertEquals(0, request8.getElapsedMs());
	}

	@Test
	public final void test_sizeOfRequests_elements() throws Exception {

		final FetchedLogsElements logs = binder.bind(
				loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedLogsElements.class);

		assertEquals(6, logs.sizeOfRequests());
	}

	@Test
	public final void test_getRequests_elements() throws Exception {

		final FetchedLogsElements logs = binder.bind(
				loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedLogsElements.class);

		final FetchedRequestElements[] requests = logs.getRequests();

		assertEquals(6, requests.length);

		assertEquals(5, requests[0].getId());
		assertEquals(8, requests[3].getId());
	}

	@Test
	public final void test_getRequests_sizeOfLogLines_elements() throws Exception {

		final FetchedLogsElements logs = binder.bind(
				loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedLogsElements.class);

		final FetchedRequestElements[] requests = logs.getRequests();

		assertEquals(6, requests.length);

		assertEquals(5, requests[0].getId());
		assertEquals(9, requests[0].sizeOfLogLines());

		assertEquals(8, requests[3].getId());
		assertEquals(12, requests[3].sizeOfLogLines());
	}

	@Test
	public final void test_getRequests_getLogLines_elements() throws Exception {

		final FetchedLogsElements logs = binder.bind(
				loadYaml("src/test/yaml/001-fetched-logs.yaml"),
				FetchedLogsElements.class);

		final FetchedRequestElements[] requests = logs.getRequests();

		assertEquals(6, requests.length);

		assertEquals(5, requests[0].getId());
		assertEquals("PHP_SELF: /tmp2/index.php",
				requests[0].getLogLines()[8].getMsgTruncated());

		assertEquals(8, requests[3].getId());
		assertEquals("PHP_SELF: /tmp2/404.php",
				requests[3].getLogLines()[8].getMsgTruncated());
	}
}
