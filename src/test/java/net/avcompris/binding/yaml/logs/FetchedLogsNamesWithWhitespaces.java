package net.avcompris.binding.yaml.logs;

import net.avcompris.binding.annotation.XPath;

import org.joda.time.DateTime;

/**
 * this interface represents log lines fetched via the Adm Web Service,
 * and differs from {@link FetchedLogs} in that property names may
 * contain whitespaces. <code>JaxenYamlBinder</code> is intended
 * for such a case.
 *
 * @author David Andrianavalontsalama
 */
@XPath("/logs")
public interface FetchedLogsNamesWithWhitespaces {

	@XPath("@count")
	int getCount();

	@XPath("requests/@count")
	int getRequestCount();

	@XPath("requests/@minId")
	int getRequestMinId();

	boolean isNullRequestMinId();

	@XPath("requests/@*[name() = 'min request at']")
	DateTime getRequestMinRequestAt();

	@XPath("requests/@maxId")
	int getRequestMaxId();

	boolean isNullRequestMaxId();

	@XPath("requests/@*[name() = 'max request at']")
	DateTime getRequestMaxRequestAt();

	@XPath("sessions/@count")
	int getSessionCount();

	@XPath("requests/*")
	FetchedRequestNamesWithWhitespaces[] getRequests();

	int sizeOfRequests();
}
