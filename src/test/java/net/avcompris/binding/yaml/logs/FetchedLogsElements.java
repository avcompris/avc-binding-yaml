package net.avcompris.binding.yaml.logs;

import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.annotation.Nodes;

import org.joda.time.DateTime;

/**
 * this interface represents log lines fetched via the Adm Web Service,
 * and differs from {@link FetchedLogs} in that properties are
 * loaded by element axis, not attributes. <code>JaxenYamlBinder</code> is intended
 * for such a case.
 *
 * @author David Andrianavalontsalama
 */
@XPath("/logs")
@Nodes(elementsEverywhere = true)
public interface FetchedLogsElements {

	@XPath("count")
	int getCount();

	@XPath("requests/count")
	int getRequestCount();

	@XPath("requests/minId")
	int getRequestMinId();

	boolean isNullRequestMinId();

	@XPath("requests/*[name() = 'minRequestAt']")
	DateTime getRequestMinRequestAt();

	@XPath("requests/maxId")
	int getRequestMaxId();

	boolean isNullRequestMaxId();

	@XPath("requests/*[name() = 'maxRequestAt']")
	DateTime getRequestMaxRequestAt();

	@XPath("sessions/count")
	int getSessionCount();

	@XPath("requests/*[contains(name(), '_item')]")
	FetchedRequestElements[] getRequests();

	int sizeOfRequests();
	
	@XPath(value="requests/*", function="name()")
	String[] getRawRequestSubs();

	int sizeOfRawRequestSubs();
}
