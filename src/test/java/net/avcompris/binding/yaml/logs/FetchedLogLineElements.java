package net.avcompris.binding.yaml.logs;

import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.annotation.Nodes;

/**
 * this interface represents a log line fetched via the Adm Web Service,
 * and differs from {@link FetchedLogLine} in that properties are
 * loaded by element axis, not attributes. <code>JaxenYamlBinder</code> is intended
 * for such a case.
 *
 * @author David Andrianavalontsalama
 */
@XPath("/logs/requests/*[contains(name(), '_item']/*[contains(name(), '_item']")
@Nodes(elementsEverywhere = true)
public interface FetchedLogLineElements {

	@XPath("range")
	int getRange();

	@XPath("level")
	char getLevel();

	@XPath("elapsedMs")
	int getElapsedMs();

	@XPath("msgTruncated")
	String getMsgTruncated();
}
