package net.avcompris.binding.yaml.misc;

import static org.junit.Assert.assertEquals;
import net.avcompris.binding.annotation.Nodes;
import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.yaml.YamlBinder;
import net.avcompris.binding.yaml.impl.JaxenYamlBinder;

import org.junit.Test;

import com.avcompris.util.SnakeYAMLUtils;
import com.avcompris.util.junit.JUnitUtils;

public class RootedListWithAttributesTest {

	@Test
	public void testRootedListWithAttributes() throws Exception {

		final YamlBinder binder = new JaxenYamlBinder();

		final RootedListWithAttributes r = binder.bind(SnakeYAMLUtils
				.loadYAML(JUnitUtils
						.createTmpFileFromCommentsAroundThisMethod()),
				RootedListWithAttributes.class);

		// title: toto
		// subs:
		//   - bla: blah
		//     age: 24
		//   - bla: blih
		//     age: 30

		assertEquals("toto", r.getTitle());
		assertEquals(2, r.sizeOfSubs());
		assertEquals("blah", r.getSubs()[0].getBla());
		assertEquals(24, r.getSubs()[0].getAge());
		assertEquals("blih", r.getSubs()[1].getBla());
		assertEquals(30, r.getSubs()[1].getAge());
	}

	@XPath("/")
	@Nodes(elementsEverywhere = true)
	private interface RootedListWithAttributes {

		@XPath("/title")
		String getTitle();

		@XPath("/subs")
		Sub[] getSubs();

		int sizeOfSubs();

		interface Sub {

			@XPath("@bla")
			String getBla();

			@XPath("@age")
			int getAge();
		}
	}
}
