package net.avcompris.binding.yaml.impl;

import static com.avcompris.util.YamlUtils.loadYAML;
import static com.avcompris.util.junit.JUnitUtils.createTmpFileFromCommentsAroundThisMethod;
import static org.apache.commons.io.FileUtils.readFileToString;
import static org.apache.commons.lang3.CharEncoding.UTF_8;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import net.avcompris.binding.BindConfiguration;

import org.custommonkey.xmlunit.XMLTestCase;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class DomYamlConverterTest {

	@Before
	public void setUp() {

		XMLUnit.setIgnoreWhitespace(true);
	}

	@After
	public void tearDown() {

		XMLUnit.setIgnoreWhitespace(false);
	}

	private static final XMLTestCase xmlTestCase = new XMLTestCase() {

		// empty
	};

	private static final BindConfiguration STANDARD_CONFIG = BindConfiguration
			.newBuilder().build();

	private static final BindConfiguration CONFIG_ELEMENTS_EVERYWHERE = BindConfiguration
			.newBuilder().setNodesElementsEverywhere(true).build();

	private static final BindConfiguration CONFIG_META_ONLY= BindConfiguration
			.newBuilder().setNodesElementsEverywhere(true).setNodesElementNames(false).build();

	@Test
	public void testSimpleYamlEqual() throws Exception {

		final Object yaml = loadYAML(createTmpFileFromCommentsAroundThisMethod(0));

		// person:
		//   name: Arthur

		final File ref = createTmpFileFromCommentsAroundThisMethod(1);

		// <person name="Arthur"/>

		assertXMLEqual(ref, yaml, STANDARD_CONFIG);

	}

	@Test
	public void testSimpleYamlNotEqual() throws Exception {

		final Object yaml = loadYAML(createTmpFileFromCommentsAroundThisMethod(0));

		// person:
		//   name: Arthur

		final File ref = createTmpFileFromCommentsAroundThisMethod(1);

		// <person name="Arthur"/>

		assertXMLNotEqual(ref, yaml, CONFIG_ELEMENTS_EVERYWHERE);
	}

	@Test
	public void testSimpleYamlElementsEverywhere() throws Exception {

		final Object yaml = loadYAML(createTmpFileFromCommentsAroundThisMethod(0));

		// person:
		//   name: Arthur

		final File ref = createTmpFileFromCommentsAroundThisMethod(1);

		// <person>
		//    <name>Arthur</name>
		// </person>

		assertXMLEqual(ref, yaml, CONFIG_ELEMENTS_EVERYWHERE);
	}

	@Test
	public void testSimpleList() throws Exception {

		final Object yaml = loadYAML(createTmpFileFromCommentsAroundThisMethod(0));

		// books:
		//   - Effective Java
		//   - Java Design Pattern Essentials

		final File ref = createTmpFileFromCommentsAroundThisMethod(1);

		// <books xmlns:bd="http://avc-binding-yaml.googlecode.com/">
		//    <bd:item>Effective Java</bd:item>
		//    <bd:item>Java Design Pattern Essentials</bd:item>
		// </books>

		assertXMLEqual(ref, yaml, STANDARD_CONFIG);
	}

	@Test
	public void testListOfMaps() throws Exception {

		final Object yaml = loadYAML(createTmpFileFromCommentsAroundThisMethod(0));

		// books:
		//   - "Effective Java": "$37" 
		//   - "Java Design Pattern Essentials": "$28"

		final File ref = createTmpFileFromCommentsAroundThisMethod(1);

		// <books xmlns:bd="http://avc-binding-yaml.googlecode.com/">
		//    <bd:item>
		//       <bd:key name="Effective Java">$37</bd:key>
		//    </bd:item>
		//    <bd:item>
		//       <bd:key name="Java Design Pattern Essentials">$28</bd:key>
		//    </bd:item>
		// </books>

		assertXMLEqual(ref, yaml, CONFIG_META_ONLY);
	}

	@Test
	public void testListOfMapsWithListsAsKeys() throws Exception {

		final Object yaml = loadYAML(createTmpFileFromCommentsAroundThisMethod(0));

		// books:
		//   - [Effective, Java]: "$37" 
		//   - "Java Design Pattern Essentials": "$28"

		final File ref = createTmpFileFromCommentsAroundThisMethod(1);

		// <books xmlns:bd="http://avc-binding-yaml.googlecode.com/">
		//    <bd:item>
		//       <bd:key>
		//          <bd:item>Effective</bd:item>
		//          <bd:item>Java</bd:item>
		//          <bd:value>$37</bd:value>
		//       </bd:key>
		//    </bd:item>
		//    <bd:item>
		//       <bd:key name="Java Design Pattern Essentials">$28</bd:key>
		//    </bd:item>
		// </books>

		assertXMLEqual(ref, yaml, CONFIG_META_ONLY);
	}

	@Test
	public void testConstraintsConfig() throws Exception {

		final Object yaml = loadYAML(new File("src/test/yaml/005-constraints.yaml"));

		final File ref = new File("src/test/xml/005-constraints.xml");

		assertXMLEqual(ref, yaml, CONFIG_META_ONLY);
	}

	private static void assertXMLEqual(final File ref, final Object yaml,
			final BindConfiguration configuration) throws IOException, SAXException, TransformerException, ParserConfigurationException {

		xmlTestCase.assertXMLEqual(readFileToString(ref, UTF_8),
				dumpNode(DomYamlConverter.yamlToNode(yaml, configuration)));

	}

	private static void assertXMLNotEqual(final File ref, final Object yaml,
			final BindConfiguration configuration) throws IOException, SAXException, TransformerException, ParserConfigurationException {

		xmlTestCase.assertXMLNotEqual(readFileToString(ref, UTF_8),
				dumpNode(DomYamlConverter.yamlToNode(yaml, configuration)));

	}

	public static String dumpNode(final Node node) throws TransformerException {

		final TransformerFactory txFactory = TransformerFactory.newInstance();

		final Transformer transformer = txFactory.newTransformer();

		transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.setOutputProperty(OutputKeys.ENCODING, UTF_8);
		transformer.setOutputProperty(
				"{http://xml.apache.org/xslt}indent-amount", "4");

		final Writer sw = new StringWriter();

		transformer.transform(new DOMSource(node), new StreamResult(sw));

		final String s = sw.toString();

		System.out.println(s);

		return s;
	}
}
