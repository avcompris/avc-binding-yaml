package net.avcompris.binding.yaml.bdkeys;

import static com.avcompris.util.junit.JUnitUtils.createTmpFileFromCommentsAroundThisMethod;
import static org.junit.Assert.assertEquals;
import net.avcompris.binding.annotation.Namespaces;
import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.yaml.YamlBinder;
import net.avcompris.binding.yaml.impl.JaxenYamlBinder;

import org.junit.Ignore;
import org.junit.Test;

import com.avcompris.util.SnakeYAMLUtils;

@Ignore
public class BdKeysListTest {

	@Test
	public void testBdKeyListOneRowOneElement() throws Exception {

		final Object yaml = SnakeYAMLUtils
				.loadYAML(createTmpFileFromCommentsAroundThisMethod());

		// person:
		// - ['aarthur']: Arthur

		final YamlBinder binder = new JaxenYamlBinder();

		final Person person = binder.bind(yaml, Person.class);

		assertEquals(1, person.sizeOfRows());

		//if (true) return;

		assertEquals(1, person.getRows()[0].sizeOfNames());

		assertEquals("aarthur", person.getRows()[0].getNames()[0]);

		assertEquals("Arthur", person.getRows()[0].getValue());
	}

	@Test
	public void testBdKeyListOneRowTwoElement() throws Exception {

		final Object yaml = SnakeYAMLUtils
				.loadYAML(createTmpFileFromCommentsAroundThisMethod());

		// person:
		// - ['aarthur', "bindkee"]: Arthur

		final YamlBinder binder = new JaxenYamlBinder();

		final Person person = binder.bind(yaml, Person.class);

		assertEquals(1, person.sizeOfRows());
		assertEquals(1, person.getRows()[0].sizeOfNames());

		assertEquals("aarthur", person.getRows()[0].getNames()[0]);

		assertEquals("Arthur", person.getRows()[0].getValue());
	}

	@Namespaces("xmlns:bd=http://avc-binding-yaml.googlecode.com/")
	@XPath("/person")
	private interface Person {

		@XPath("*")
		Row[] getRows();

		int sizeOfRows();

		interface Row {

			@XPath("bd:key/bd:value")
			String getValue();

			@XPath("* | bd:key")
			String[] getNames();

			int sizeOfNames();
		}
	}
}
