package net.avcompris.binding.yaml.bdkeys;

import static com.avcompris.util.junit.JUnitUtils.createTmpFileFromCommentsAroundThisMethod;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import net.avcompris.binding.annotation.Namespaces;
import net.avcompris.binding.annotation.XPath;
import net.avcompris.binding.yaml.YamlBinder;
import net.avcompris.binding.yaml.impl.JaxenYamlBinder;

import org.junit.Test;

import com.avcompris.util.SnakeYAMLUtils;

public class BdKeysMapTest {

	@Test
	public void testBdKeyMapOneElement() throws Exception {

		final Object yaml = SnakeYAMLUtils
				.loadYAML(createTmpFileFromCommentsAroundThisMethod());

		// person:
		//   {id: 'aarthur'}: Arthur

		final YamlBinder binder = new JaxenYamlBinder();

		final Person person = binder.bind(yaml, Person.class);

		assertEquals("aarthur", person.getId());

		assertEquals("Arthur", person.getFirstName());
	}

	@Test
	public void testBdKeyListOneElement() throws Exception {

		final Object yaml = SnakeYAMLUtils
				.loadYAML(createTmpFileFromCommentsAroundThisMethod());

		// person:
		//   [id: 'aarthur']: Arthur

		final YamlBinder binder = new JaxenYamlBinder();

		final Person person = binder.bind(yaml, Person.class);

		assertEquals("aarthur", person.getId());
		assertEquals("Arthur", person.getFirstName());
	}

	@Namespaces("xmlns:bd=http://avc-binding-yaml.googlecode.com/")
	@XPath("/person")
	private interface Person {

		@XPath("bd:key/@id")
		String getId();

		@XPath("*")
		String getFirstName();

		@XPath("bd:key/bd:value/@age")
		int getAge();

		@XPath("bd:key/bd:value/@male")
		boolean isMale();

		@XPath("bd:key/bd:value/@lastName")
		String getLastName();
	}

	@Test
	public void testBdKeyMapOneElementMap() throws Exception {

		final Object yaml = SnakeYAMLUtils
				.loadYAML(createTmpFileFromCommentsAroundThisMethod());

		// person:
		//   {id: arthurm}: {age: 90, male: true, lastName: Miller}

		final YamlBinder binder = new JaxenYamlBinder();

		final Person person = binder.bind(yaml, Person.class);

		assertEquals("arthurm", person.getId());
		assertEquals("Miller", person.getLastName());
		assertEquals(90, person.getAge());
		assertTrue(person.isMale());
	}
}
