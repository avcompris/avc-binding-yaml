package net.avcompris.jaxen.yaml;

interface YamlWithProperties {

	String[] getPropertyNames();

	Object getObjectProperty(String name);
}
