package net.avcompris.jaxen.yaml;

import static com.avcompris.util.ExceptionUtils.nonNullArgument;
import static org.apache.commons.lang3.StringUtils.isBlank;

import java.util.Date;
import java.util.List;
import java.util.Map;

import net.avcompris.logging.AvcLog;

import org.apache.commons.logging.LogFactory;

import com.avcompris.common.annotation.Nullable;
import com.avcompris.lang.NotImplementedException;
import com.avcompris.util.AbstractUtils;

abstract class YamlNodes extends AbstractUtils {

	private static final AvcLog log = new AvcLog(
			LogFactory.getLog(YamlNodes.class));

	public static boolean isSimpleValue(final Object object) {

		if (log.isDebugEnabled()) {
			log.debugf("isSimpleValue");
		}

		nonNullArgument(object, "object");

		final Class<?> c = object.getClass();

		if (String.class.equals(c) || Character.class.equals(c)
				|| c.isPrimitive() || Boolean.class.equals(c)
				|| Number.class.isAssignableFrom(c)
				|| Date.class.isAssignableFrom(c)) {

			if (log.isDebugEnabled()) {
				log.debugf("  => isSimpleValue: true");
			}

			return true;
		}

		if (YamlNode.class.isAssignableFrom(c) || Map.class.isAssignableFrom(c)
				|| List.class.isAssignableFrom(c)) {

			if (log.isDebugEnabled()) {
				log.debugf("  => isSimpleValue: false");
			}

			return false;
		}

		throw new NotImplementedException("c: " + c);
	}

	public static boolean isEmptyAttributeValue(@Nullable final Object value) {

		if (value == null) {

			return true;
		}

		if (value instanceof String && isBlank((String) value)) {

			return true;
		}

		return false;
	}
}
