package net.avcompris.logging;

import java.util.Collection;
import java.util.Map;

import org.apache.commons.logging.Log;

import com.avcompris.common.annotation.Nullable;

/**
 * wrapper for {@link Log} that adds a convenient {@link #debugf(Object...)}
 * method, with debugging messages being enriched and truncated.
 * 
 * @author David Andrianavalontsalama
 */
public class AvcLog extends LogWrapper {

	public AvcLog(final Log log) {

		super(log);
	}

	public void debugf(@Nullable final Object... objects) {

		if (objects == null) {

			log.debug("");

			return;
		}

		final StringBuilder sb = new StringBuilder();

		for (final Object o : objects) {

			final String s = String.valueOf(o);

			if (o != null) {

				if (o.getClass().isArray()) {

					sb.append("length=").append(((Object[]) o).length)
							.append(":");

				} else if (Collection.class.isInstance(o)) {

					sb.append("size=").append(((Collection<?>) o).size())
							.append(":");

				} else if (Map.class.isInstance(o)) {

					sb.append("size=").append(((Map<?, ?>) o).size())
							.append(":");
				}
			}

			sb.append(s.length() > 80 ? s.substring(0, 80) + "..." : s);
		}

		log.debug(sb);
	}
}
