package net.avcompris.logging;

import static com.avcompris.util.ExceptionUtils.nonNullArgument;

import org.apache.commons.logging.Log;

import com.avcompris.common.annotation.Nullable;

/**
 * wrapper for {@link Log}.
 * 
 * TODO move this class to another project, since it is not YAML-specific.
 * 
 * @author David Andrianavalontsalama
 */
public abstract class LogWrapper implements Log {

	public LogWrapper(final Log log) {

		this.log = nonNullArgument(log, "log");
	}

	protected final Log log;

	@Override
	public void debug(final @Nullable Object message) {
		log.debug(message);
	}

	@Override
	public void debug(final @Nullable Object message,
			final @Nullable Throwable t) {

		log.debug(message, t);
	}

	@Override
	public void error(final @Nullable Object message) {
		log.error(message);
	}

	@Override
	public void error(final @Nullable Object message,
			final @Nullable Throwable t) {
		log.error(message, t);
	}

	@Override
	public void fatal(final @Nullable Object message) {
		log.fatal(message);
	}

	@Override
	public void fatal(final @Nullable Object message,
			final @Nullable Throwable t) {
		log.fatal(message, t);
	}

	@Override
	public void info(final @Nullable Object message) {
		log.info(message);
	}

	@Override
	public void info(final @Nullable Object message, final @Nullable Throwable t) {
		log.info(message, t);
	}

	@Override
	public boolean isDebugEnabled() {
		return log.isDebugEnabled();
	}

	@Override
	public boolean isErrorEnabled() {
		return log.isErrorEnabled();
	}

	@Override
	public boolean isFatalEnabled() {
		return log.isFatalEnabled();
	}

	@Override
	public boolean isInfoEnabled() {
		return log.isInfoEnabled();
	}

	@Override
	public boolean isTraceEnabled() {
		return log.isTraceEnabled();
	}

	@Override
	public boolean isWarnEnabled() {
		return log.isWarnEnabled();
	}

	@Override
	public void trace(final @Nullable Object message) {
		log.trace(message);
	}

	@Override
	public void trace(final @Nullable Object message,
			final @Nullable Throwable t) {
		log.trace(message, t);
	}

	@Override
	public void warn(final @Nullable Object message) {
		log.warn(message);
	}

	@Override
	public void warn(final @Nullable Object message, final @Nullable Throwable t) {
		log.warn(message, t);
	}
}
