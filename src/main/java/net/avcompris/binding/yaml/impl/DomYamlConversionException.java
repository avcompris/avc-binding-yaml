package net.avcompris.binding.yaml.impl;

import com.avcompris.common.annotation.Nullable;

/**
 * this exception is thrown when a YAML incoming stream could not be converted to a DOM node, due to
 * illegal XML names for instance.
 * 
 * @author David Andrianavalontsalama
 */
public class DomYamlConversionException extends RuntimeException {

	/**
	 * for serialization.
	 */
	private static final long serialVersionUID = -3673163637099960658L;

	/**
	 * constructor.
	 */
	public DomYamlConversionException(
			@Nullable final String message,
			@Nullable final Throwable cause) {

		super(message, cause);
	}
}
