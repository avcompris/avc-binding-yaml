package net.avcompris.binding.yaml.impl;

public interface YamlContent {

	String getTextContent();
}
