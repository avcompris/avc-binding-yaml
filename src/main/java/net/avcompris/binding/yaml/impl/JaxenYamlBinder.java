package net.avcompris.binding.yaml.impl;

import static com.avcompris.util.ExceptionUtils.nonNullArgument;

import java.lang.reflect.Proxy;

import net.avcompris.binding.BindConfiguration;
import net.avcompris.binding.impl.AbstractBinder;
import net.avcompris.binding.impl.AbstractBinderInvocationHandler;
import net.avcompris.binding.yaml.YamlBinder;

/**
 * an implementation of {@link YamlBinder} that uses the Jaxen XPath evaluation mechanism.
 *
 * @author David Andrianavalontsalama
 */
public class JaxenYamlBinder extends AbstractBinder<Object> implements
		YamlBinder {

	@Override
	protected <T> T bindInterface(final BindConfiguration configuration,
			final Object yaml, final ClassLoader classLoader,
			final Class<T> clazz) {

		nonNullArgument(clazz, "clazz");
		nonNullArgument(yaml, "yaml");
		nonNullArgument(configuration, "configuration");
		nonNullArgument(classLoader, "classLoader");

		final AbstractBinderInvocationHandler<Object> invocationHandler = new JaxenYamlBinderInvocationHandler(
				this, classLoader, clazz, yaml, configuration);

		final Object proxy = Proxy.newProxyInstance(classLoader,
				new Class<?>[]{
					clazz
				}, invocationHandler);

		return clazz.cast(proxy);
	}
}
