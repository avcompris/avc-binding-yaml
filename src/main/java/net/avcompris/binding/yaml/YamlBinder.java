package net.avcompris.binding.yaml;

import net.avcompris.binding.BindConfiguration;
import net.avcompris.binding.Binder;

/**
 * a Binder is responsible for binding a Java dynamix proxy to a YAML incoming stream.
 *
 * @author David Andrianavalontsalama
 */
public interface YamlBinder extends Binder<Object> {

	/**
	 * bind a Java dynamic proxy to a YAML incoming stream,
	 * using the XPath expression declared as an annotation in the interface.
	 */
	@Override
	<T> T bind(Object yaml, Class<T> clazz);

	/**
	 * bind a Java dynamic proxy to a YAML incoming stream,
	 * using the XPath expression declared as an annotation in the interface
	 * and the {@link ClassLoader} passed as a parameter.	 
	 */
	@Override
	<T> T bind(Object yaml, ClassLoader classLoader, Class<T> clazz);

	/**
	 * bind a Java dynamic proxy to a YAML incoming stream,
	 * using the XPath expression passed as a parameter.
	 */
	@Override
	<T> T bind(BindConfiguration configuration, Object yaml, Class<T> clazz);

	/**
	 * bind a Java dynamic proxy to a YAML incoming stream,
	 * using the XPath expression
	 * and the {@link ClassLoader} passed as a parameter.
	 */
	@Override
	<T> T bind(BindConfiguration configuration, Object yaml,
			ClassLoader classLoader, Class<T> clazz);
}
